package com.example.demo1.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public class StudentDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StudentDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    List<Student> selectAllStudents() {
        String sql = "" +
                "SELECT " +
                " studentId, " +
                " firstName, " +
                " lastName, " +
                " email " +
                "FROM Student";

        return jdbcTemplate.query(sql, mapStudentFomDb());
    }
    int insertStudent(UUID studentId, Student student) {
        String sql = "" +
                "INSERT INTO student (" +
                " studentId, " +
                " firstName, " +
                " lastName, " +
                " email) " +
                "VALUES (?, ?, ?, ?)";
        return jdbcTemplate.update(
                sql,
                studentId,
                student.getFirstName(),
                student.getLastName(),
                student.getEmail()
        );
    }
    @SuppressWarnings("ConstantConditions")
    boolean isEmailTaken(String email) {
        String sql = "" +
                "SELECT EXISTS ( " +
                " SELECT 1 " +
                " FROM student " +
                " WHERE email = ?" +
                ")";
        return jdbcTemplate.queryForObject(
                sql,
                new Object[]{email},
                (resultSet, i) -> resultSet.getBoolean(1)
        );
    }
    private RowMapper<Student> mapStudentFomDb() {
        return (resultSet, i) -> {
            String studentIdStr = resultSet.getString("student_id");
            UUID studentId = UUID.fromString(studentIdStr);

            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String email = resultSet.getString("email");

            return new Student(
                    studentId,
                    firstName,
                    lastName,
                    email
            );
        };
    }

    int updateEmail(UUID studentId, String email) {
        String sql = "" +
                "UPDATE Student " +
                "SET email = ? " +
                "WHERE studentId = ?";
        return jdbcTemplate.update(sql, email, studentId);
    }

    int updateFirstName(UUID studentId, String firstName) {
        String sql = "" +
                "UPDATE Student " +
                "SET firstName = ? " +
                "WHERE studentId = ?";
        return jdbcTemplate.update(sql, firstName, studentId);
    }

    int updateLastName(UUID studentId, String lastName) {
        String sql = "" +
                "UPDATE Student " +
                "SET lastName = ? " +
                "WHERE studentId = ?";
        return jdbcTemplate.update(sql, lastName, studentId);
    }

    @SuppressWarnings("ConstantConditions")
    boolean selectExistsEmail(UUID studentId, String email) {
        String sql = "" +
                "SELECT EXISTS ( " +
                "   SELECT 1 " +
                "   FROM Student " +
                "   WHERE studentId <> ? " +
                "    AND email = ? " +
                ")";
        return jdbcTemplate.queryForObject(
                sql,
                new Object[]{studentId, email},
                (resultSet, columnIndex) -> resultSet.getBoolean(1)
        );
    }

    int deleteStudentById(UUID studentId) {
        String sql ="DELETE FROM Student WHERE studentId = ?";

        return jdbcTemplate.update(sql, studentId);
    }



}
