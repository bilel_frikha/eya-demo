package com.example.demo1.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Student")
public class StudentEntity {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy="uuid2")
    @Column(name = "studentId")
    private  UUID studentId;

    @Column(name = "firstName")
    private  String firstName;
    @Column(name = "lastName")
    private  String lastName;
    @Column(name = "email")
    private  String email;

    public StudentEntity(UUID studentId, String firstName, String lastName, String email) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;

    }
    public StudentEntity() {

    }

    public UUID getStudentId() {
        return studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

}
